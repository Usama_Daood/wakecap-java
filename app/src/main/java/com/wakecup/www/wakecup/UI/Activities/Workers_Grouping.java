package com.wakecup.www.wakecup.UI.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.chootdev.recycleclick.RecycleClick;
import com.wakecup.www.wakecup.Adapters.All_Roles;
import com.wakecup.www.wakecup.Adapters.Worker_Adapter;
import com.wakecup.www.wakecup.DataModel.Workers;
import com.wakecup.www.wakecup.R;

import java.util.ArrayList;
import java.util.List;

public class Workers_Grouping extends AppCompatActivity {
    private ArrayList<String> array_role_name,array_all_workers_names;
    private ArrayList<Integer> array_role_ids, array_all_role_id;
    RecyclerView selectedrecycler,recycler_view_worker_role_wise;
    private Worker_Adapter adapter;
    List<Workers> worker_list;
    TextView res;
    int num=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workers__grouping);
        array_role_ids = new ArrayList<Integer>();
        array_all_role_id = new ArrayList<Integer>();
        array_role_name = new ArrayList<String>();
        array_all_workers_names=new ArrayList<String>();
        worker_list=new ArrayList<>();

        //dataList= new List<Workers>();
        selectedrecycler = findViewById(R.id.selected_player_recycler);
        res=findViewById(R.id.result);
        recycler_view_worker_role_wise=findViewById(R.id.recycler_view_worker_role_wise);

        Bundle extras = getIntent().getExtras();
        if(extras !=null)
        {

            array_role_name= extras.getStringArrayList("array_role_names");
            array_role_ids = extras.getIntegerArrayList("array_role_ids");
            array_all_role_id= extras.getIntegerArrayList("array_all_role_id");
            array_all_workers_names=extras.getStringArrayList("array_all_workers_names");
        }



        selectedrecycler.setLayoutManager(new LinearLayoutManager(Workers_Grouping.this, LinearLayoutManager.HORIZONTAL, false));
        selectedrecycler.setAdapter(new All_Roles(array_role_name, array_role_ids, Workers_Grouping.this));

        RecycleClick.addTo(selectedrecycler).setOnItemClickListener(new RecycleClick.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                num=0;
                res.setText("");
                worker_list.clear();
                array_role_ids.get(position);
                int role_id=array_role_ids.get(position);
                for(int i=0;i<array_all_role_id.size();i++){
                    if(role_id==array_all_role_id.get(i)){
                        num=num+1;
                        res.append(num+". "+array_all_workers_names.get(i)+" \n ");
                        Workers worker=new Workers(
                                array_all_workers_names.get(i),
                                0,
                                array_role_name.get(position)
                        );
                        worker_list.add(worker);
                    }


                }
                /// Set Adapter
                recycler_view_worker_role_wise.setLayoutManager(new LinearLayoutManager(Workers_Grouping.this, LinearLayoutManager.VERTICAL, false));
                adapter = new Worker_Adapter(worker_list, Workers_Grouping.this);
                recycler_view_worker_role_wise.setAdapter(adapter);
                /// End Adapter
            }
        });
    }
}
