package com.wakecup.www.wakecup.Utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.net.wifi.WifiManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.Toast;

import com.wakecup.www.wakecup.R;

public class Utils_methods {

    public static void on_off_wifi(final Context context){

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setTitle("Info");
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setMessage(R.string.network_error_msg);
        alertDialogBuilder.setPositiveButton("Turn on Wifi",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                        try {
                            WifiManager wifi;
                            wifi=(WifiManager) context.getSystemService(Context.WIFI_SERVICE);
                            wifi.setWifiEnabled(true);//Turn on Wifi
                            Toast.makeText(context, "Turning on wifi.", Toast.LENGTH_SHORT).show();




                        }catch (Exception b){
                            Log.d("",""+b.toString());
                        }
                    }

                });
        alertDialogBuilder.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ((Activity)context).finish();

            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
        }


    public static void Check_errors_in_API(final Context context, final int status_code){
        String msg= context.getResources().getString(R.string.server_error_msg);
        String button_name = "Ok";
        if(status_code==500){
            ///// Internal Server Error ////////
            msg= context.getResources().getString(R.string.server_error_msg);

        }else if(status_code==502){
///////////////// Show 502 Bad Gateway error  ////////////////
            msg= context.getResources().getString(R.string.server_error_msg);

        }else if(status_code==504){
            /////////// 504 Gateway Timeout error ////////
            msg= context.getResources().getString(R.string.server_error_msg);

        }else if(status_code==404) {
            ////////====>  HTTP 404 Not Found Error  <===

            msg= context.getResources().getString(R.string.server_error_msg);

        } else if (status_code == 422) {
            ////// If User token is not generated.
            msg = context.getResources().getString(R.string.server_error_msg);

        } else if (status_code == 401) {
            /// When token is expires

        }



        ////////// Alert Dialogue


        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setTitle("Info");

        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setMessage(msg);
        alertDialogBuilder.setPositiveButton(button_name,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {





                    }

                });


        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }

}
