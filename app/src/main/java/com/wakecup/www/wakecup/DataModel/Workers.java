package com.wakecup.www.wakecup.DataModel;

public class Workers {

    String Worker_Name;
    int Worker_id;
    String worker_role;

    public Workers(String Worker_Name,int Worker_id,String worker_role){
        this.Worker_Name=Worker_Name;
        this.Worker_id=Worker_id;
        this.worker_role=worker_role;
    }



    public int getWorker_id() {
        return Worker_id;
    }

    public String getWorker_role() {
        return worker_role;
    }

    public String getWorker_Name() {
        return Worker_Name;
    }


}
