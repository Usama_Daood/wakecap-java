package com.wakecup.www.wakecup.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wakecup.www.wakecup.DataModel.Workers;
import com.wakecup.www.wakecup.R;

import java.util.List;

public class Worker_Adapter extends RecyclerView.Adapter<Worker_Adapter.NoticeViewHolder> {

    private List<Workers> dataList;
    Context context;
//    public Worker_Adapter(ArrayList<Workers> dataList) {
//        this.dataList = dataList;
//    }

    public Worker_Adapter(List<Workers> dataList, Context context) {
        this.dataList =  dataList;
        this.context=context;
    }

    @Override
    public NoticeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.worker_row, parent, false);
        return new NoticeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(NoticeViewHolder holder, int position) {
        holder.txtNoticeTitle.setText(dataList.get(position).getWorker_Name());
        holder.txtNoticeBrief.setText(dataList.get(position).getWorker_role());
//        holder.txtNoticeFilePath.setText(dataList.get(position).getFileSource());
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    class NoticeViewHolder extends RecyclerView.ViewHolder {

        TextView txtNoticeTitle, txtNoticeBrief, txtNoticeFilePath;

        NoticeViewHolder(View itemView) {
            super(itemView);
            txtNoticeTitle =  itemView.findViewById(R.id.txt_notice_title);
            txtNoticeBrief =  itemView.findViewById(R.id.txt_notice_brief);
            txtNoticeFilePath =  itemView.findViewById(R.id.txt_notice_file_path);
        }
    }




}
