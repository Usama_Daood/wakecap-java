package com.wakecup.www.wakecup.UI.Activities;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.wakecup.www.wakecup.Adapters.Worker_Adapter;
import com.wakecup.www.wakecup.DataModel.Workers;
import com.wakecup.www.wakecup.R;
import com.wakecup.www.wakecup.Utils.Contants;
import com.wakecup.www.wakecup.Utils.Utils_methods;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
	RequestQueue requestQueue;
	String token="Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiUlNBLU9BRVAiLCJraWQiOiIzaGltWWEyYmJHS18zSVdZMUxUUHdyTUl2ZkF3ZVhZQmRNLTU5XzN6c0MwIn0.DIwk0Q215_M7rt3iGfyRVrImhKCXvEB5NBoLQj6iqBnN9yCWWBhGW9fmX6beOajtWgC8gpupe1hJ-iurFRbo2Eg52Nh9ljkkO5Mz8ulSpH7YlfP8Yi-TBrNoPIf_IvHrr1Rt8bG0w2Ie1Jo3BBOguMQ6Q2WI4fQDY-oFIluxOQ5t9E0_XvDhna7maiQnWevLbu80Mj7g643v5nvNWOdpSAXW91rZXIXdal_A_ffY-IUyvYhEJuOkyHWwNonuGjiGHWKPTjTR16w3seoSs4srkacOIN1cmMdXKlkvlWSYCv5Nd8xJ_rTqA4GIyKLRSALYcuiTFMOIxeMIpPB5DjpMTOmOD72QnD8MHMx-SNHk2Yva5iPs__h_WXIKzjME6wRQxTBayfWcXuA6loTdSXnV5Tfl63y62nYZUesh2ElcF5ZK_if3R70jTh-_dxAc8-EcxagAgeCAYNGx-i0fokA4Crfz0A9MKJv5XKn8hY-_4_AT-tsIwBQ6YWYISPBGqyUFoBX0LGDEojIywQBXcLSqHRoah45IDW87TTjVTtPjKsxrIr9rl661dYKRE4ViTLGON0hRBmmNAOokcTNv4tEPBWDS5OPIVtnqkCruAV1YXGKMWpbGOlH6pi2sLY5FtNP9h7nNKnK59gX_FMwXyLjzdPU7HkEEv2svTHN_vBFDUeE.UmwA_rn0FGSRXl4kVPrF3Q.lpZ6JQSke2wLCi3X18mNMAYs5x2nCk7LGrs65nOo-cv8FQ1zfYz9qj7TdH26BxmIrtJiBlLkLhJJnWmSjceNsQnCaOFt36cStJKTHbRkydezuBJyIgvlQH-8Y3nTXLpIbo3JQjxTJ-lUwmY8bLnyxS91DaKEjmip3DoXJLSa4uOivuC68npCCtPVFeHbdHrzS_wQTRsUEh1o0XtWI1TtW868mV3njXwtjHdKxp7Uk8wfY5i5R4qJHxXWgf8PuXJA99Hh1Jwzee_tuA67wnPxvKf95mFkZVYZdDqNKrFkDE9B7-r5FjLi3pQZdTMXjFBGAMv20jrM5BuvCZLP9p6ugbG5pOBRUjBT5RtQpw48_th6pTa4W36MkBkh5TbXS27yuOXvi-b3blcSCEiizEDcJvqm4DBwqCNlSR8iG_QgktFDzkG99--LHV4R956K6cAozVAeyv0Jo9mKfaTsRLlMNhfTFpPrZmR5hVb11CEmmLi9C84aK1eivlYOZenzdZKpYE2mBKjZXOl96PJ3jb-OwEO2MDw9fyGzez8NaVhWAGGw9rm1iXnQyGljOw_ufOr5GLPHXXMhjSGVpXbN65sCTv8p6XGZnJiW7otVUzzmxG_kYQ3tkFLTyG9p6SG3GGBDVYG_pkDpPWavWlQdplIDTsOrjoXu-8sAYTx6GO_rGL13haW7KY9pRi9v3324zkNYOYL8CWWZzjcm26607lTEhpVWvkP4QwisGjvrAMoyj-GnlKvyK1u-1sPcuRMUg-8AKRYzBdtlChoUOtdVwYsOHxiLAkRMBH4PRIqWyUNILmlugXGNA7umZxj8uWy2mKjpZ7M8D_YlR66tj4duARp0lJYfeuvDqbXABVuSoLs8qaEFuYXLfiTNH4zItNeMQjfzT_eh9S_ligNALdOb82FgZzYv5ZKbFMo2s9kwzAb4PdwyaZjIP4_UJiJBqOazslWJYpdMLY2ZHGwfg0ZFP3adV06hbR1i9bJ7lyew2Dalqh9Sq-cFPg32-6rbZXYJnFFzfMks4FOJCgPOhCz2QODdbZU8_nQp_EQ4mIXnD2BR6BcCa3odD8rliM3IYOWnq3AhCHen55FwygCw6-u61Q03m4httHFozE-x4YiQivTQ70YUmdYNMyU_chQ3WfiJzaOgEwjq8vIP0hasxiaE1fBG7PncjGCUNB9sFiyVYjmSbP68iIQFqQ10oo9hnMYVxs_nSdem23fDNOutoikzjSHPo4_qxg-hNV0GDIrzBPLGsGw_W9agnUQOpwtpYycZ9v472FWhI5z0c8-8U7D9cV9s0ELyO9U7_vHhSoZJCq1edIFVVKTHsVfR2-vOlCHuqlbH4TzqGtIzf-5nUuY7HQ4iIxdjOrfU2GtMaU_tB8v3ZsC2Nr7IMYeyeBhItOXccNnqHUhMyZ-LdHkoafendmWTnnMOlkK2l91Jil4tPrJyVn1YFlsbzobQOurrsz2MAW3Ew1ibTCsRvlKWRs8dOSAxoUBcQL-r9g5_BcF0wsV7xzWvIHFm7fOuqLGsjsrCMAuXKImA2tFYECpr6vqCq8ORdqapUOlMb0_K4Iae-AhEkbMVERgLS3O05tRhQATMl2jnqA71DImzDoQMC_c45ELXToqX7x0oiINsOLQKBFQjiQ4-G9C1DJlZ67ZVtSN2PyKVNqMNegEtSz1uieOmkoXa1Vq62CrDp7KORToTHMZEV9uWe7KAOzqYm7jwhEvFs2jgvSga9Au2YgzqxbBjpC8RDSQqhUZTQeg2gecFLRpIObQ._YXzEw0gT4HbLqcFfdWoFQ";
	String url="https://pilot.wakecap.com/api/sites/10010001/workers/";
	private Worker_Adapter adapter;
	private RecyclerView recyclerView;
	JSONArray jsonArray;
	JSONObject JsonArray_2;
	List<Workers> worker_list;
	TextView loading;
	Toolbar toolbar;
	private ProgressBar progressBar;
	RecyclerView Worker_recycler;
	LottieAnimationView animationView;
    private ArrayList<String> array_role_name,array_all_workers_names;
    private ArrayList<Integer> array_role_id;
    private ArrayList<Integer> array_all_role_id;
    int statusCode;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		animationView = findViewById(R.id.loading_lottie);
		loading = findViewById(R.id.loading);
		progressBar= findViewById(R.id.progressBar_cyclic);
		Worker_recycler=findViewById(R.id.recycler_view_notice_list);
		Worker_recycler.setLayoutManager(new LinearLayoutManager(this));
		worker_list=new ArrayList<>();

        array_role_name = new ArrayList<String>();
        array_all_workers_names=new ArrayList<String>();
        array_role_id = new ArrayList<Integer>();
        array_all_role_id=new ArrayList<Integer>();

        animationView.setColorFilter(Color.RED);
		requestQueue = Volley.newRequestQueue(this);
		get_data();


	}

	public void show_progress(){
		loading.setVisibility(View.VISIBLE);
	}
	public void hide_progress(){
		loading.setVisibility(View.GONE);
		getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
		animationView.setVisibility(View.GONE);
	}

	public void get_data(){
	    show_progress();
		JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.GET, Contants.API_LINK, null,
					new Response.Listener<JSONObject>() {
						@Override
						public void onResponse(JSONObject response) {
							Log.d("Response", "" + response.toString());
							try {
								JSONObject jsonObj = new JSONObject(String.valueOf(response));
								JSONObject contact = jsonObj.getJSONObject("data");
								JSONArray userJson = contact.getJSONArray("items");

								userJson.getString(0);
								Log.d("New Res",""+userJson.toString());
								Log.d("Names",""+userJson.toString()+" "+userJson.getString(6));
								JSONObject obj = new JSONObject(String.valueOf(contact));
								jsonArray = obj.getJSONArray("items");
								int count = 0;
								for (int i = 0; i < userJson.length(); i++) {
								   JSONObject jo = jsonArray.getJSONObject(count);
								   JsonArray_2 = jo.getJSONObject("attributes");
								   String name= JsonArray_2.getString("full_name");
								   String role = JsonArray_2.getString("role");
								   int id=JsonArray_2.getInt("id");
									JSONObject Obj_Role=JsonArray_2.getJSONObject("Role");
                                    JSONObject js= Obj_Role.getJSONObject("attributes");
								   js.getString("name");
								   Obj_Role.getString("type");
								   Workers worker=new Workers(
										   JsonArray_2.getString("full_name"),
										   JsonArray_2.getInt("id"),
										   JsonArray_2.getString("role")
								   );
                                    array_all_role_id.add(js.getInt("id"));
                                    array_all_workers_names.add(JsonArray_2.getString("full_name"));
								   worker_list.add(worker);
                                    if(!array_role_id.contains(js.getInt("id"))){
                                        array_role_name.add(js.getString("name"));
                                        array_role_id.add(js.getInt("id"));
                                    }
                                    count=count+1;

							   }
							   ///// Add adapter

								adapter = new Worker_Adapter(worker_list, MainActivity.this);
								Worker_recycler.setAdapter(adapter);
								hide_progress();
							} catch (JSONException e) {
								try{
									e.printStackTrace();
									hide_progress();
								}catch (Exception t){
                                    hide_progress();
								}

							}
						}
					},
					new Response.ErrorListener() {
						@Override
						public void onErrorResponse(VolleyError error) {
						    hide_progress();
							try {
								statusCode = error.networkResponse.statusCode;
								Log.d("", "" + statusCode);
								//  Toast.makeText(getApplicationContext(), error.getMessage()+" Code "+statusCode, Toast.LENGTH_SHORT).show();
							} catch (Exception b) {

							}


                            if (error instanceof TimeoutError) {
                                //This indicates that the reuest has either time out or there is no connection
                              hide_progress();
                              Utils_methods.Check_errors_in_API(MainActivity.this, statusCode);
                            } else if (error instanceof AuthFailureError) {
                                hide_progress();
                                Utils_methods.Check_errors_in_API(MainActivity.this, statusCode);
							} else if (error instanceof ServerError) {
                                hide_progress();
                                Utils_methods.Check_errors_in_API(MainActivity.this, statusCode);
                            } else if (error instanceof NetworkError) {
                                hide_progress();
                                Utils_methods.Check_errors_in_API(MainActivity.this, statusCode);
                            } else if (error instanceof ParseError) {
                                hide_progress();
                                Utils_methods.Check_errors_in_API(MainActivity.this, statusCode);
                                } else if (error instanceof NoConnectionError) {
                                hide_progress();
                                Utils_methods.Check_errors_in_API(MainActivity.this, statusCode);
                                 }






                        }
					}) {
				/**
				 * Passing some request headers*
				 */
				@Override
				public Map getHeaders() {
					HashMap headers = new HashMap();
					headers.put("Content-Type", "application/json");
					headers.put("Authorization", Contants.API_TOKEN);
					headers.put("X-Requested-With","XMLHttpRequest");

					return headers;
				}
			};

			int socketTimeout = 3000;
			RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
			postRequest.setRetryPolicy(policy);

			requestQueue.add(postRequest);





	}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home_menu, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.reload) {
            Intent myIntent = new Intent(MainActivity.this, Workers_Grouping.class);
            myIntent.putExtra("array_role_names", array_role_name);
            myIntent.putExtra("array_role_ids", array_role_id);
            myIntent.putExtra("array_all_role_id", array_all_role_id);
            myIntent.putExtra("array_all_workers_names", array_all_workers_names);
            MainActivity.this.startActivity(myIntent);

        }

return false;
    }




    }
